# Code Quality Integration

Happens to use Python Flake8 Code Quality tool but is also an example for plugging custom code quality into Merge Requests.

## Example Extension Use
[Example project that uses this extension](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/calling-extensions-examples/flask-autodevops-with-integrated-custom-code-quality)
